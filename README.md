# SETR2_Display2

Displays LCD gráficos para sistemas empotrados.

Objetivos del proyecto:

Controlar un display LCD usando el STM32: 
–Texto.
–Gráficos estáticos.
–Gráficos dinámicos.

Diseño e implementación de tareas que muestren distintos gráficos interactivos.
-Creación y eliminación de tareas dinámicamente.
-Lectura del ADC mediante DMA.


Display LCD
El display LCD-TFT basado en el controlador SSD1289:
–Tamaño: 3.2 pulgadas
–Resolución espacial: 320 horizontal X 240 Vertical – Resolución color: 16bits (RGB565)
–Touch Panel: Resistivo, con interfaz SPI


-Alimentaciones: 3.3V / 5V
-Bus de datos: D0..D15 (16bits)
-Un solo bit de dirección: RS
-Líneas de control:
-Chip Select (CS)
-Read/Write ( RD y WR)
-Bus SPI: touch panel.


El módulo del display lo integran tres componentes:
1.El display TFT con película resistiva.
2.El controlador del display: SSD1289
3.Interfaz Touch Panel: ADS7843


La codificación RGB-565
-El display tiene 16 bits de resolución de color y usa la codificación RGB-565.
-Los 16 bits del color empaquetan el color usando la codificación 565:
    .Los 5 bits más significativos contienen el rojo (32 niveles de rojo).
    .Los 6 bits siguientes contienen el verde (64 niveles de verde).
    .Los 5 bits menos significativos contienen el azul (32 niveles de azul).


Funcionalidad:
1.Representar figuras en el display.
2.Simulación de un objeto (coche) para que avance 10 pixeles hacia la derecha. El refresco de este movimiento es un valor modificable.


Componentes:
    
- FRANCISCO JAVIER EXPOSITO BAUTISTA (https://gitlab.com/Fraexpbau/setr2_display2.git)
- ANDREW GONZALEZ PYRTEK (https://gitlab.com/Andr_w/setr2_display2.git)

